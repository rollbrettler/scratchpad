#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import requests
from blinkt import set_pixel, set_brightness, show, clear

set_brightness(0.5)

while True:
    tina = requests.get("http://192.168.0.23:8123/api/states/device_tracker.34fcefa4d450")
    tim = requests.get("http://192.168.0.23:8123/api/states/device_tracker.78f882d068d")
    on = requests.get("http://192.168.0.23:8123/api/states/input_boolean.notify_home")
    clear()
    if tina.json()['state'] == "home" and on.json()['state'] == "on":
        for i in range(0,4):
            set_pixel(i, 249, 0, 249)

    if tim.json()['state'] == "home" and on.json()['state'] == "on":
        for i in range(4,8):
            set_pixel(i, 0, 255, 0)

    if tina.json()['state'] == "home" and tim.json()['state'] == "home" and on.json()['state'] == "on":
        for i in range(8):
            set_pixel(i, 255, 63, 0)
    show()
    time.sleep(5)
