a = fork do
  Signal.trap("HUP") { puts "Ouch!"; exit }
  sleep 10
  puts "😓" # won’t happen
end

Process.detach(a)

begin
  sleep 5
ensure
  Process.kill("HUP", a)
end
